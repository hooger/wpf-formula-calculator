﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Stoichiometry;

namespace StoichiometryClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Molecule molecule;
        
        public MainWindow()
        {
            InitializeComponent();            
            try
            {
                molecule = new Molecule();
                cmbFormula.ItemsSource = molecule.Molecules;
                lstElements.ItemsSource = molecule._symbols;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(99);
            }            
        }

        
        
        private void btnClose_Click(object sender, RoutedEventArgs e) 
        {
            cmbFormula.Text = "";
            txtWeight.Text = "";
        }

        private void btnNormalize_Click(object sender, RoutedEventArgs e)
        {
            if (cmbFormula.Text != "")
            {
                molecule.Formula = cmbFormula.Text;
                if (!molecule.Valid())
                {
                    MessageBox.Show("The Formula: (" + cmbFormula.Text + ") is not a valid formula.", "Not Valid", MessageBoxButton.OK, MessageBoxImage.Error);
                    cmbFormula.Text = "";
                }
                else
                {
                    molecule.Normalize();
                    cmbFormula.Text = molecule.Formula;
                }
            }

        }

        private void btnCalc_Click(object sender, RoutedEventArgs e)
        {
            if (cmbFormula.Text != "")
            {
                molecule.Formula = cmbFormula.Text;
                if (!molecule.Valid())
                {
                    MessageBox.Show("The Formula: (" + cmbFormula.Text + ") is not a valid formula.", "Not Valid", MessageBoxButton.OK, MessageBoxImage.Error);
                    cmbFormula.Text = "";
                }
                else
                {
                    txtWeight.Text = molecule.MolecularWeight.ToString();
                    cmbFormula.Text = molecule.Formula;

                    if (MessageBox.Show("Would you like to save your formula to the database?", "Save Dialog", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                    {
                        if (!molecule.Save())
                            MessageBox.Show("Formula already exists.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        else
                        {
                            cmbFormula.ItemsSource = null;
                            cmbFormula.ItemsSource = molecule.Molecules;
                        }

                    }
                }
            }
        }

        private void cmbFormula_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            molecule.Formula = cmbFormula.Text;
            txtWeight.Text = molecule.MolecularWeight.ToString();
            cmbFormula.Text = molecule.Formula;
        }

        private void cmbFormula_TextInput(object sender, TextCompositionEventArgs e)
        {
            
        }

        private void cmbFormula_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void lstElements_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            cmbFormula.Text += lstElements.SelectedValue;
            
        }
                
    }
}
