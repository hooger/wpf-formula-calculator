﻿/*
 *  Project:        Stoichiometry
 *  Module:         Element.cs
 *  Date:           Feb 9th, 2014
 *  Author:         Jordon de Hoog
 *  Description:    Element object, complements Molecule.cs.  This object
 *                  stores all the information about the element.  Also has
 *                  two different constructors depending on the needs.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stoichiometry
{
    public class Element
    {
        public short AtomicNumber   { get; private set; }
        public double Weight        { get; private set; }
        public string Symbol        { get; private set; }
        public string Name          { get;  set; }

        /// <summary>
        /// Element(short,double,string,string):
        ///     Four argument constructor.
        ///     See Molecule.cs - initElements() for usage
        /// </summary>
        public Element(short aN, double w, string s, string n)
        {
            AtomicNumber = aN;
            Weight = w;
            Symbol = s;
            Name = n;
        }

        /// <summary>
        /// Element(string):
        ///     One argument constructor.
        ///     See Molecule.cs - MolecularWeight for usage
        /// </summary>
        /// <param name="s">
        ///     The symbol of the element
        /// </param>
        public Element(string s)
        {
            Symbol = s;
        }
    }
}
