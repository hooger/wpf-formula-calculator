﻿/*
 *  Project:        Stoichiometry
 *  Module:         Molecule.cs
 *  Date:           Feb 7th, 2014
 *  Author:         Jordon de Hoog
 *  Description:    Contains all the needed functions to interact with an excel client
 *                  or a wpf/wcf client.  It will pull elemental data from a database,
 *                  and calculate the weight of a users formula.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace Stoichiometry
{
    public interface IMolecule
    {
        string Formula { get; set; }
        bool Valid();
        double MolecularWeight { get; }
        void Normalize();
        bool Save();
        string[] FormulasList { get; }
    }
    
    public class Molecule : IMolecule
    {
        public Molecule()
        {
            if (!initElements())
                throw new Exception("Could not initiate connection with database, locate the database file");
        }

        // Data-related member variables 
        private DataSet _ds;
        private OleDbDataAdapter _adElements, _adMolecules;
        public List<Element> _elements = new List<Element>();
        public List<string> _molecules = new List<string>();
        public List<string> _symbols = new List<string>();
        public string _molecule;        
        public double _moleculeWeight;
        public Dictionary<string,int> _elementsFoundInFormula = new Dictionary<string,int>();
        
        /// <summary>
        /// initElements:
        ///     This monstrosity of a method will set up the inital database objects, and will
        ///     than load up the global variables with all the values.
        ///     List<Element> _elements: This contains a list of all elements pulled from
        ///                              the database.
        ///     List<string> _symbols:   This contains a list of all the symbols of each element.
        ///     List<string> _molecules: This holds all the saved molecules from the database.     
        /// </summary>
        /// <returns>
        ///     <bool>isInitialized</bool> returns and sets isInitialized if the method completed
        ///     successfully.
        /// </returns>
        public bool initElements()
        {
            //Always wrap database actions in a try catch.
            try
            {
                //Create connection to db and create data adapter for elements table
                OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Stoichiometry.accdb");
                _adElements = new OleDbDataAdapter();
                _adElements.SelectCommand = new OleDbCommand("SELECT * FROM Elements", conn);

                //Create data adapter for molecules table
                _adMolecules = new OleDbDataAdapter();
                _adMolecules.SelectCommand = new OleDbCommand("SELECT * FROM Molecules", conn);
                _adMolecules.InsertCommand = new OleDbCommand("INSERT INTO Molecules (Formula,MolecularWeight) VALUES(@Formula,@MolecularWeight)", conn);
                _adMolecules.InsertCommand.Parameters.Add("@Formula", OleDbType.VarChar, -1, "Formula");
                _adMolecules.InsertCommand.Parameters.Add("@MolecularWeight", OleDbType.VarChar, -1, "MolecularWeight");
                
                //Create and fill the data set and close the connection 
                _ds = new DataSet();
                conn.Open();
                _adElements.Fill(_ds, "Elements");
                _adMolecules.Fill(_ds, "Molecules");
                conn.Close();

                //Populate list of elements
                foreach (DataRow row in _ds.Tables["Elements"].Rows)
                {
                    _elements.Add(new Element(
                            row.Field<short>("AtomicNumber"),
                            row.Field<double>("AtomicWeight"),
                            row.Field<string>("Symbol"),
                            row.Field<string>("Name")                        
                        ));
                    _symbols.Add(row.Field<string>("Symbol"));
                }

                //Populate list of molecules
                foreach (DataRow row in _ds.Tables["Molecules"].Rows)
                {
                    _molecule = row.Field<string>("Formula");
                    _molecules.Add(_molecule);
                    _molecule = ""; //reset - prevent future errors
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        /// <summary>
        /// Formula:
        ///     Accessor for the formula attribute
        /// </summary>
        public string Formula
        {
            get { return _molecule; }
            set { _molecule = value; }
        }

        public List<string> Molecules
        {
            get { return _molecules; }
        }

        /// <summary>
        /// Valid:
        ///     Will check if the users molecule(formula) contains legal characters.
        ///     It will then check whether or not each of the elements exists in the 
        ///     database List<string>_symbols.  If it does exist it will add the
        ///     element and the number of the element to a 
        ///     dictionary<string,int> (_elementsFoundInFormula)
        ///     if that element already exists it will increment the number of them.
        /// </summary>
        /// <returns>
        ///     If it fails any of the tests: Illegal characters, element doesn't exist,
        ///     then it will return false
        /// </returns>
        public bool Valid()
        {
            string elementRegex = "([A-Z][a-z]*)([0-9]*)";
            string validateRegex = "^(" + elementRegex + ")+$";

            //Test 1:  Illegal characters
            if (!Regex.IsMatch(_molecule, validateRegex))
                return false;

            foreach (Match match in Regex.Matches(_molecule, elementRegex))
            {
                string symbol = match.Groups[1].Value;
                int count = match.Groups[2].Value != "" ?
                            int.Parse(match.Groups[2].Value) : 1;

                //Test 2: Element actualy exists
                if (!_symbols.Contains(symbol))
                    return false;

                //Test 3: If matched elements exist, increment instead of add
                if (_elementsFoundInFormula.ContainsKey(symbol))
                    _elementsFoundInFormula[symbol] += count;
                else
                    _elementsFoundInFormula.Add(symbol, count);
            }
            //All good return true
            return true;
        }

        /// <summary>
        /// MolecularWeight:
        ///     This accessor method will normalize the users formula, which will
        ///     place the individual elements into a nicer format (Dictionary<string,int>)
        ///     It will then loop through that dictionary of elements. Each loop
        ///     an Element() object will be created using the element symbol.
        ///     It will then loop through all of the elements in the
        ///     List<Elements> _elements until it finds one with a matching symbol.
        ///     It will then grab the weight of that match and multiply it by the value
        ///     from the Dictionary<string,int>, then add it to the running total.
        /// </summary>
        /// <returns>
        ///     The weight <double>_moleculeWeight</double> which is the running total of
        ///     the weights of each individual element
        /// </returns>
        public double MolecularWeight
        {
            get 
            {
                _moleculeWeight = 0.0;  //House keeping
                Normalize();            //Get the elements in a nice dictionary

                foreach (var entry in _elementsFoundInFormula)
                {
                    Element temp = new Element(entry.Key);
                    foreach (Element e in _elements)
                    {
                        if (temp.Symbol == e.Symbol)                        
                            _moleculeWeight += e.Weight * entry.Value;                        
                    }
                }
                return _moleculeWeight;
            }
        }

        /// <summary>
        /// Normalize:
        ///     First call Valid() to do most of the heavy lifting.  Valid() will
        ///     extract all the individual elements and place them in a dictionary.
        ///     Normalize will take this dictionary and create a single string from
        ///     the key, value pairs.
        /// </summary>
        public void Normalize()
        {
            _elementsFoundInFormula.Clear();
            if (Valid())
            {
                _molecule = ""; //House keeping
                foreach (var element in _elementsFoundInFormula)
                    _molecule += element.Value == 1 ? element.Key : element.Key + element.Value;
            }
        }

        /// <summary>
        /// Save:
        ///     If the molecule to be saved already exists then skip saving, if not then
        ///     assign the correct variables to the row.  Than save to the database.
        /// </summary>
        /// <returns>
        ///     Return false if the molecule already exists
        /// </returns>
        public bool Save()
        {
            if (_molecules.Contains(_molecule))
                return false;
            else
                _molecules.Add(_molecule);
           
            DataRow moleculeToSave = _ds.Tables["Molecules"].NewRow();
            moleculeToSave["Formula"] = _molecule;
            moleculeToSave["MolecularWeight"] = _moleculeWeight;
            _ds.Tables["Molecules"].Rows.Add(moleculeToSave);
            _adMolecules.Update(_ds, "Molecules");

            return true;
        }

        /// <summary>
        /// FormulasList:
        ///     converts the list of molecules (formulas) from the List<string> _molecules
        ///     into the needed string[] of molecules
        /// </summary>
        public string[] FormulasList
        {
            get 
            {
                //convert list to string
                string[] formulas = _molecules.ToArray();
                return formulas; 
            }
        }    
    }//end Molecule
}//end namespace
