# README #

Fanashawe College
Semester 5
Component Programming .NET

**Project 1 - Formula Calculator**

### What is this repository for? ###

* Calculator takes a non-normalized formula, will normalize it and then calculate the molecular weight of the molecule.  It will then add it to an access database
* 1.0.0

### How do I get set up? ###

* Clone
* Access database file needs to be in "My Documents"
* Build using Visual Studio 2012

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact